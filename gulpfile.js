/*
 *
 * Определяем переменные
 *
 */
var gulp        = require('gulp'),
    browserSync = require('browser-sync');



// Задача "image". Запускается командой "gulp image"
// gulp.task('image', function() {
//   gulp.src(['app/img/**/*.*'])
//     .pipe(gulp.dest('app/img'))
// });


gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: 'app'
        },
        notify: false
    });
});


gulp.task('watch', ['browser-sync'],function () {
    // gulp.watch(['app/img/**'], ['image']);
    gulp.watch('app/*.html', browserSync.reload);

});

